<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use APY\DataGridBundle\Grid\Source\Entity;
use Symfony\Component\HttpFoundation\Request;
use TestBundle\Form\ProductType;

class DefaultController extends Controller
{
    /**
     * @Route("/products", name="products_grid")
     * @Template()
     */
    public function gridAction()
    {


        $source = new Entity('TestBundle:Product');

        $grid = $this->get('grid');
        $grid->setSource($source);

        return [
            'grid' => $grid
        ];

    }

    /**
     * @Route("/add")
     * @Template()
     */
    public function addAction(Request $request)
    {

        $form = $this->createForm(new ProductType());
        $form->handleRequest($request);

        if ($form->isValid()) {

            $product = $form->getData();

            $this->getDoctrine()
                ->getManager()
                ->persist($product);

            $this->getDoctrine()
                ->getManager()
                ->flush();

            return $this->redirectToRoute('products_grid');
        }

        return [
            'form' => $form->createView()
        ];

    }
}
