<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use APY\DataGridBundle\Grid\Source\Entity;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use TestBundle\Entity\Product;

/**
 * @Route("/api")
 */
class ApiController extends Controller
{
    /**
     * Get all products view
     *
     * @Route("/", defaults={"_format" = "json"})
     * @Route("/{_format}", requirements={"_format" = "json|yml"}, defaults={"_format" = "json"})
     * @Method("GET")
     *
     */
    public function productsAction($_format)
    {

        $products = $this->getDoctrine()->getRepository('TestBundle:Product')->findAll();


        $data =  $this->get('jms_serializer')->serialize($products, $_format);

        return new Response($data);
    }

    /**
     * Get product by ID
     * @Route("/{id}/{_format}", requirements={"id" = "\d+", "_format" = "json|yml"})
     * @Method("GET")
     */
    public function productAction(Product $product, $_format)
    {
        $data =  $this->get('jms_serializer')->serialize($product, $_format);

        return new Response($data);
    }
}
