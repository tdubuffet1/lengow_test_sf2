<?php

namespace TestBundle\Service;

use Monolog\Logger;
use Symfony\Component\DomCrawler\Crawler;
use TestBundle\Entity\Product;

/**
 * Created by PhpStorm.
 * User: Tdubuffet
 * Date: 07/02/2017
 * Time: 12:23
 */
class CrawlerLengow
{

    private $logger;
    private $urlOrders;

    /**
     * LoadProductXml constructor.
     *
     * @param Logger $logger
     * @param string $urlOrders Url of orders XML
     */
    public function __construct(Logger $logger, $urlOrders)
    {
        $this->logger       = $logger;
        $this->urlOrders    = $urlOrders;
    }

    /**
     * Load XML
     *
     * @return string XML
     * @throws \Exception
     */
    public function loadXML()
    {
        $this->logger->addInfo('[Crawler] Get remote file ');

        $curl = curl_init($this->urlOrders);

        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_TIMEOUT        => 50,
        ]);

        $response = curl_exec($curl);

        if($response === false) {
            $this->logger->addError('[Crawler] Curl error: ' . curl_error($curl));
            throw new \Exception('Curl error: ' . curl_error($curl));
        }


        $this->logger->addInfo('[Crawler] OK !');

        return $response;
    }

    /**
     * @param $fluxXml
     * @return array Product
     */
    public function crawlProducts($fluxXml)
    {

        $crawler = new Crawler($fluxXml);

        $products = [];

        $crawler->filter('statistics > orders > order')->each(function (Crawler $node, $i) use (&$products) {

            $node->filter('cart > products > product')->each(function (Crawler $node, $i) use (&$products) {
                $products[] = $this->xmlToProduct($node);
            });

        });

        return $products;

    }

    /**
     * @param Crawler $node
     * @return Product
     */
    private function xmlToProduct(Crawler $node)
    {

        $title          = $node->filter('title')->text();
        $sku            = $node->filter('sku')->text();
        $quantity       = $node->filter('quantity')->text();
        $image          = $node->filter('url_image')->text();
        $price          = $node->filter('price')->text();
        $categoryName   = $node->filter('category')->text();


        $product = new Product();
        $product->setTitle($title)
                ->setSku($sku)
                ->setQuantity($quantity)
                ->setCategory($categoryName)
                ->setImage($image)
                ->setPrice($price);


        $this->logger->addInfo('[Crawler] Find Product: ' . $product->getTitle() .' - Sku: ' . $product->getId());

        return $product;
    }

}