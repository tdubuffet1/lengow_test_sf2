<?php

namespace TestBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Created by PhpStorm.
 * User: Tdubuffet
 * Date: 13/02/2017
 * Time: 10:14
 */
class CrawlerLengowCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('crawling:lengow')
            ->setDescription('Add product at lengow with remote file xml')
            ->setHelp(
                <<<EOT
                    The <info>%command.name%</info>command create user grade.

                    <info>php %command.full_name%</info>

EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $response = $this->getContainer()
            ->get('lengow_test')
            ->loadXML();

        $products = $this->getContainer()
            ->get('lengow_test')
            ->crawlProducts($response);

        foreach($products as $product) {

            $exist = $this->getContainer()
                ->get('doctrine')
                ->getRepository('TestBundle:Product')
                ->findBySku($product->getSku());

            if (!$exist) {
                $this->getContainer()->get('doctrine')->getManager()->persist($product);
            }

        }

        $this->getContainer()->get('doctrine')->getManager()->flush();


    }

}